# Why use machine learning on PBC

Partisia Blockchain's support for running user-defined secure multiparty computations (MPC) makes it a natural candidate for performing privacy-preserving machine learning (PPML) inference. Using [zero knowledge (ZK) smart contracts](../zk-smart-contracts/zk-smart-contracts.md), the owner of a pre-trained machine learning (ML) model can make it available for any user who wants to run the model on an input sample to obtain an output &mdash; all while keeping the model, the input sample, and the output, fully private.

Machine learning (ML) models are used in a wide range of applications. Inference is the process of running the model on an input sample and producing an output. Inference requires information sharing: either the model owner sends their model to the sample owner, who then runs it on their sample to obtain an output, or the sample owner sends their sample to the model owner, who then generates and sends back the output. Neither of these scenarios are optimal. For one, the model constitutes intellectual property, so the model owner may not wish to share it. Even if they do, the model may require regular updates, which results in distribution overhead. Secondly, the sample owner's input may consist of highly sensitive data, which they do not wish to share in the open.

A ZK smart contract on PBC solves this issue. A model owner can create a ZK smart contract that specifies (using the [ZK Rust language](../zk-smart-contracts/zk-rust-language-zkrust.md)) how to run the model on an input sample and then upload their pre-trained model to the contract. The model is [secret-shared](https://medium.com/partisia-blockchain/mpc-techniques-series-part-1-secret-sharing-d8f98324674a), so none of the nodes involved in the computations will learn any private information. Likewise, when a sample owner subsequently interacts with the contract, their input sample will be secret-shared upon upload. Inference is performed on secret shares, which guarantees that no information is revealed about the model or the input. Depending on the specifications of the smart contract, the output can be kept secret from everyone except the intended receiver.

In this section, you can explore how to use ZK smart contracts on PBC to run PPML models.

First, we present an example of a ZK smart contract that performs private inference on a binary decision tree classifier, including an explanation of the imagined use case behind the contract and how you can adapt it to your own needs. A decision tree classifier is just one of many powerful ML algorithms with a wide range of practical applications.

Second, we show what to keep in mind in terms of the capabilities of PBC and the [ZK Rust language](../zk-smart-contracts/zk-rust-language-zkrust.md) if you want to create your own ZK smart contract that uses a different ML algorithm.

Last, we explain how to deploy your ZK smart contract on the [testnet](https://browser.testnet.partisiablockchain.com/transactions) and how to interact with it through the browser, including how to upload the model and the input sample and how to retrieve the output.

## Content

-   [Example of a ZK smart contract for running a decision tree classifier](../machine-learning/ppml-example-contract.md)
-   [How to create and deploy a ZK smart contract for your own machine learning model](../machine-learning/make-your-own-ppml-contract.md)
-   [How to interact with your smart contract through the browser](../machine-learning/interact-with-ppml-smart-contract.md)
