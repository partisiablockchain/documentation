# How to interact with your smart contract

If you have written a ZK smart contract to perform inference on a machine learning model, you can test it on our [testnet](https://browser.testnet.partisiablockchain.com/transactions) for free before deploying it on the [PBC mainnet](https://browser.partisiablockchain.com/blocks). In this section, we have gathered everything you need to know about deploying a contract &mdash; from getting access to the testnet, compiling a contract and uploading the contract files, interacting with the contract through the browser, and retrieving the inference output.

## Deploy on testnet

You can jump to [this section](../access-and-use-the-testnet.md) to read about how to get access to the testnet. Once you have enough test gas, you can read about [how to compile a ZK smart contract](../zk-smart-contracts/compile-and-deploy-zk-contract.md). After you have compiled the contract and signed in to the testnet, you must upload the ABI-file and the ZKWA-file to deploy the contract. Remember to add enough gas for the contract to run.

![Upload ML ZK smart contract files](../img/machine-learning-upload-contract-00.png)

## Interact with the contract

Once the contract has been successfully deployed, you can interact with it. Any metadata associated with the contract will be public on the testnet.

### Upload model and input sample

As the contract owner, you can now upload your model to the contract. As we explained in the [previous section](../machine-learning/make-your-own-ppml-contract.md), the upload process depends on how you represent the model in the smart contract.

![Interact with ML ZK smart contract](../img/machine-learning-interact-with-contract-00.png)

After the model has been uploaded, the contract is ready to accept input samples. You must ensure that the input sample owner knows the correct order in which to add the feature values. In our [example contract](../machine-learning/ppml-example-contract.md), we allow the input sample owner to specify the receiver of the inference output. The address of the receiver is submitted with the feature values when uploading the input sample.

![Add input sample to smart contract](../img/machine-learning-add-sample-00.png)

As soon as an input sample has been uploaded, the ZK computation will automatically begin. When the computation is complete, the output will be handled in accordance with the specifications of the contract. For example, you can choose to have the output be transferred to the specified receiver and remain secret until the receiver potentially chooses to open it, or you can specify that it must be opened to publicly reveal the result. If the computation was successful, the contract will look like this:

![ML successful ZK computation](../img/machine-learning-successful-interaction-00.png)

### Retrieve inference output

If your contract specifies that the secret-shared inference output is transferred to the specified receiver, it can only be accessed by the receiver through the command line. To do so, they must be signed in with their private key and then run the following command:

```bash
cargo pbc contract secret show <contract-address> <output-variable-id> <output-type>
```

The output of the command will be `secret: <inference result>`.
